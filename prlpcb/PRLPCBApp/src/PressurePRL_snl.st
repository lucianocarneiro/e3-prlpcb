program program_snl
 /*option +r;*/
option +r; /*Make re-entrant*/

%% #include <stdlib.h>
%% #include <stddef.h>
%% #include <string.h>
%% #include <ctype.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <epicsString.h>
%% #include <epicsStdio.h>


int manual;
int valve_op;
int close_all;
int stat;

float pressure;
float setpoint;
float threshold;
float t_a;
float t_b;
float t_c;

assign manual "{P}{R}Manual-Op-S";
assign valve_op "{P}{R}Valve-Op-S";
assign close_all "{P}{R1}Close_All";

assign pressure "{P}{R}Pressure-R";
assign setpoint "{P}{R}Pressure-S";
assign threshold "{P}{R}MinPressure";
assign t_a "{P}{R}DelayA";
assign t_b "{P}{R}DelayB";
assign t_c "{P}{R}DelayC";
assign stat "{P}{R}Status";


monitor manual;
monitor valve_op;

monitor pressure;
monitor setpoint;
monitor threshold;

monitor t_a;
monitor t_b;
monitor t_c;


ss valve {
    state init {
        entry {
            stat=0;
            pvPut(stat);
            }
        when() {
            pvGet(pressure);
            pvGet(manual);
            } state delaymode
    }
    state delaymode {
        entry {
            stat=1;
            pvPut(stat);
            }
        when(manual==0) {
            pvGet(pressure);
            stat=2;
            } state wait_min
        when(manual==1) {
            stat=7;
            } state delay_tc
        }
    state delay_tc {
        entry {
            stat=7;
            pvPut(stat);
            }
        when(delay(t_c)) {
            stat=1;
            } state wait
        }
    state min_press {
        when(pressure >= threshold) {
            stat=4;
            } state wait_set
        when(pressure < threshold) {
            //printf("Close all the valves \n");
            //printf("Systems unsealead! \n");
            close_all=1;
            pvPut(close_all);
            stat=3;
            } state wait
        }
    state set_point {
        when(pressure >= setpoint) {
            } state delay_tc
        when(pressure < setpoint) {
            valve_op=1;
            pvPut(valve_op);
            } state delay_ta
        }
    state delay_ta {
        entry {
            stat=5;
            pvPut(stat);
            }
        when(delay(t_a)) {
            } state delay_tb
        }
    state delay_tb {
        entry {
            stat=6;
            pvPut(stat);
            }
        when(delay(t_b)) {
            valve_op=0;
            pvPut(valve_op);
            stat=1;
            } state wait
        }
    state wait {
        entry {
            pvPut(stat);
            }
        when(delay(0.5)) {
            } state delaymode
        }
    state wait_min {
        entry {
            pvPut(stat);
            }
        when(delay(0.5)) {
            } state min_press
        }
    state wait_set {
        entry {
            pvPut(stat);
            }
        when(delay(0.5)) {
            } state set_point
        }
}